import React from 'react';
import { Modal, Platform } from 'react-native';

import { BoxArea, Box, Header, Text, Body } from './styled';

export default function ModalDefault({ visible, children, closeAction }) {
  return (
    <Modal visible={visible} transparent={true} animationType="fade">
      <BoxArea behavior={Platform.OS === 'ios' ? 'padding' : null}>
        <Box>
          <Header onPress={closeAction} underlayColor="transparent">
            <Text>X</Text>
          </Header>
          <Body>{children}</Body>
        </Box>
      </BoxArea>
    </Modal>
  );
}

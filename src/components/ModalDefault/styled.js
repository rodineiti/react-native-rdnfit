import styled from 'styled-components/native';

export const BoxArea = styled.KeyboardAvoidingView`
  flex: 1;
  background-color: rgba(0, 0, 0, 0.5);
  justify-content: center;
  align-items: center;
`;

export const Box = styled.View`
  width: 90%;
  padding: 20px;
  background-color: #fff;
`;

export const Header = styled.TouchableHighlight`
  height: 40px;
  align-self: flex-end;
`;

export const Text = styled.Text`
  font-size: 25px;
`;

export const Body = styled.View``;

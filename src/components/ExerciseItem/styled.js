import styled from 'styled-components/native';

export const AreaTouchable = styled.TouchableHighlight`
  height: 50px;
  flex-direction: row;
  background-color: #fff;
  margin-bottom: 10px;
`;

export const MuscleArea = styled.View`
  width: 50px;
  height: 50px;
  background-color: #ffcc98;
  border-radius: 10px;
  justify-content: center;
  align-items: center;
`;

export const Image = styled.Image`
  width: 35px;
  height: 35px;
`;

export const Info = styled.View`
  flex-direction: column;
  justify-content: center;
  margin-left: 5px;
`;

export const Name = styled.Text`
  font-size: 15px;
  color: #000;
`;

export const Details = styled.Text`
  font-size: 15px;
  color: #999;
`;

export const Swipe = styled.TouchableHighlight`
  height: 50px;
  background-color: #ff0000;
  justify-content: center;
`;

export const Icon = styled.Image`
  width: 20px;
  height: 20px;
  margin-left: 15px; // 50 de altura do swipe - 20 de altura deste = 30 / 2 = 15
`;

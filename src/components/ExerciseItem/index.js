import React from 'react';
import useImageMuscle from '../useImageMuscle';
import { SwipeRow } from 'react-native-swipe-list-view';

import {
  AreaTouchable,
  MuscleArea,
  Image,
  Info,
  Name,
  Details,
  Swipe,
  Icon,
} from './styled';

export default function ExerciseItem({ data, delAction, editAction }) {
  return (
    <SwipeRow leftOpenValue={50} disableLeftSwipe={true}>
      <Swipe onPress={delAction} underlayColor="#ff0000">
        <Icon source={require('./../../assets/trash-white.png')} />
      </Swipe>
      <AreaTouchable onPress={editAction} underlayColor="#fff">
        <>
          <MuscleArea>
            <Image source={useImageMuscle(data.muscle)} />
          </MuscleArea>
          <Info>
            <Name>{data.name}</Name>
            <Details>
              {`${data.sets} séries - ${data.reps} rep ${
                data.load ? `- ${data.load} kg` : ''
              }`}
            </Details>
          </Info>
        </>
      </AreaTouchable>
    </SwipeRow>
  );
}

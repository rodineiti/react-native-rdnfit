import React, { useState, useEffect, useRef } from 'react';
import { Dimensions } from 'react-native';
import Day from '../Day';

import { Scroll } from './styled';

const width = Math.round(Dimensions.get('window').width);

let dayW = Math.round(width / 9);

let offsetW = Math.round((width - dayW) / 2);

export default function DaysScroll({
  day,
  month,
  setDay,
  dailyProgress,
  workoutDays,
}) {
  const dayRef = useRef();
  const [daySelected, setDaySelected] = useState(day);

  let days = [];
  let daysInMonth = new Date(new Date().getFullYear(), month + 1, 0).getDate(); // ultimo dia do mes anterior

  for (let i = 1; i <= daysInMonth; i++) {
    days.push(i);
  }

  const handleScrolEnd = (event) => {
    let positionX = event.nativeEvent.contentOffset.x;
    let target = Math.round(positionX / dayW) + 1;
    setDaySelected(target);
  };

  const scrolToDay = (day) => {
    let positionX = (day - 1) * dayW;
    dayRef.current.scrollTo({
      x: positionX,
      y: 0,
      animated: true,
    });
  };

  useEffect(() => {
    setDay(daySelected);
  }, [daySelected]);

  useEffect(() => {
    setTimeout(() => {
      if (month === new Date().getMonth()) {
        scrolToDay(new Date().getDate());
      } else {
        scrolToDay(1);
      }
    }, 10);
  }, [month]);

  return (
    <Scroll
      ref={dayRef}
      horizontal={true}
      showsHorizontalScrollIndicator={false}
      snapToInterval={dayW}
      contentContainerStyle={{
        paddingLeft: offsetW,
        paddingRight: offsetW,
      }}
      onMomentumScrollEnd={handleScrolEnd}
      decelerationRate="fast"
    >
      {days.map((item, key) => (
        <Day
          key={key}
          day={item}
          month={month}
          dailyProgress={dailyProgress}
          workoutDays={workoutDays}
          dayW={dayW}
          onPress={() => scrolToDay(item)}
        />
      ))}
    </Scroll>
  );
}

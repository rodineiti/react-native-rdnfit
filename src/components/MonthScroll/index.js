import React, { useState, useEffect, useRef } from 'react';
import { Dimensions } from 'react-native';

import { Scroll, Button, Item, Text } from './styled';

const months = [
  'Janeiro',
  'Fevereiro',
  'Março',
  'Abril',
  'Maio',
  'Junho',
  'Julho',
  'Agosto',
  'Setembro',
  'Outubro',
  'Novembro',
  'Dezembro',
];
const width = Math.round(Dimensions.get('window').width);
// pegar um terço da largura da tela do dispositivo
let thirdW = Math.round(width / 3);

export default function MonthScroll({ month, setMonth }) {
  const monthRef = useRef();
  const [monthSelected, setMonthSelected] = useState(month);

  const handleScrolEnd = (event) => {
    let positionX = event.nativeEvent.contentOffset.x;
    let targetMonth = Math.round(positionX / thirdW);
    setMonthSelected(targetMonth);
  };

  const scrolToMonth = (month) => {
    let positionX = month * thirdW;
    monthRef.current.scrollTo({
      x: positionX,
      y: 0,
      animated: true,
    });
  };

  useEffect(() => {
    setMonth(monthSelected);
  }, [monthSelected]);

  useEffect(() => {
    setTimeout(() => {
      scrolToMonth(monthSelected);
    }, 10);
  }, [month]);

  return (
    <Scroll
      ref={monthRef}
      horizontal={true}
      showsHorizontalScrollIndicator={false}
      snapToInterval={thirdW}
      contentContainerStyle={{
        paddingLeft: thirdW,
        paddingRight: thirdW,
      }}
      onMomentumScrollEnd={handleScrolEnd}
      decelerationRate="fast"
    >
      {months.map((item, key) => (
        <Button
          key={key}
          width={thirdW}
          onPress={() => setMonthSelected(key)}
          underlayColor="transparent"
        >
          <Item
            style={
              key === monthSelected
                ? {
                    backgroundColor: '#999',
                    width: '100%',
                    height: 40,
                  }
                : {}
            }
          >
            <Text>{item}</Text>
          </Item>
        </Button>
      ))}
    </Scroll>
  );
}

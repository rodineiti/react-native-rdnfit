import styled from 'styled-components/native';

export const Scroll = styled.ScrollView`
  width: 100%;
  height: 60px;
`;

export const Button = styled.TouchableHighlight`
  width: ${(props) => props.width}px;
  justify-content: center;
  align-items: center;
`;

export const Item = styled.View`
  width: 90%;
  height: 30px;
  background-color: #ccc;
  border-radius: 15px;
  justify-content: center;
  align-items: center;
`;

export const Text = styled.Text``;

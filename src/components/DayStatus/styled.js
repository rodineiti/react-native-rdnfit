import styled from 'styled-components/native';

export const BallonTriangle = styled.View`
  width: 0;
  height: 0;
  border-left-color: transparent;
  border-right-color: transparent;
  border-left-width: 15px;
  border-right-width: 15px;
  border-bottom-width: 15px;
  border-bottom-color: #ccc;
`;

export const BallonArea = styled.View`
  width: 90%;
  padding: 20px;
  background-color: #ccc;
  border-radius: 10px;
`;

export const BigText = styled.Text`
  font-size: 15px;
  align-self: center;
`;

export const Text = styled.Text`
  color: #fff;
  font-weight: bold;
`;

export const SmallText = styled.Text`
  font-size: 13px;
  align-self: center;
  margin-top: 10px;
`;

export const Bold = styled.Text`
  font-weight: bold;
`;

import React, { useState, useEffect } from 'react';

import {
  BallonTriangle,
  BallonArea,
  BigText,
  Text,
  SmallText,
  Bold,
} from './styled';

import ButtonDefault from './../ButtonDefault';

export default function DayStatus({
  day,
  month,
  dailyProgress,
  workoutDays,
  addProgress,
  delProgress,
  goToWorkout,
}) {
  const [time, setTime] = useState('');
  let today = new Date();
  today.setHours(0);
  today.setMinutes(0);
  today.setSeconds(0);
  today.setMilliseconds(0);

  let thisDate = new Date(today.getFullYear(), month, day);

  let thisYear = thisDate.getFullYear();
  let thisMonth = thisDate.getMonth() + 1;
  let thisDay = thisDate.getDate();
  if (thisMonth < 10) '0' + thisMonth;
  if (thisDay < 10) '0' + thisDay;

  let dateFormat = `${thisYear}-${thisMonth}-${thisDay}`;

  let dayOff = false;
  let isToday = false;
  let isFuture = false;
  let isDone = false;

  // se não contem nos dias de treino definidos, eh dia de descanso
  if (!workoutDays.includes(thisDate.getDay())) {
    dayOff = true;
  } else if (thisDate.getTime() > today.getTime()) {
    // se for no futuro, ou seja, o dia de hoje seja maior
    isFuture = true;
  } else {
    // se foi ou não feito o treino
    isDone = dailyProgress.includes(dateFormat) ? true : false;
  }

  if (thisDate.getTime() === today.getTime()) {
    isToday = true;
  }

  useEffect(() => {
    const timerFunc = () => {
      let now = Date.now();
      let endToday = new Date();
      endToday.setHours(23);
      endToday.setMinutes(59);
      endToday.setSeconds(59);
      endToday.setHours(23);
      endToday = endToday.getTime();

      let diff = endToday - now;

      let h = Math.floor(diff / (1000 * 60 * 60));
      let m = Math.floor(diff / (1000 * 60) - h * 60);
      let s = Math.floor(diff / 1000 - m * 60 - h * 60 * 60);

      h = h < 10 ? '0' + h : h;
      m = m < 10 ? '0' + m : m;
      s = s < 10 ? '0' + s : s;

      setTime(`${h}h ${m}m ${s}s`);
    };
    let timer = setInterval(timerFunc, 1000);
    timerFunc();

    return () => clearInterval(timer); // quando sair dela tela, ele encerra o timer
  }, []);

  return (
    <>
      <BallonTriangle></BallonTriangle>

      <BallonArea>
        {dayOff && (
          <BigText>
            <Bold>Dia de descanso</Bold>
          </BigText>
        )}
        {isFuture && (
          <BigText>
            <Bold>Dia no futuro</Bold>
          </BigText>
        )}
        {!dayOff && !isFuture && isDone && (
          <>
            <BigText>
              <Bold>Parabéns</Bold>, você treinou!
            </BigText>
            <ButtonDefault
              bgcolor="#4AC34E"
              style={{ marginTop: 20 }}
              onPress={() => delProgress(dateFormat)}
              underlayColor="transparent"
            >
              <Text>DESMARCAR</Text>
            </ButtonDefault>
          </>
        )}
        {!dayOff && !isFuture && !isDone && !isToday && (
          <>
            <BigText>
              <Bold>Que pena!</Bold> Neste dia você não treinou :(
            </BigText>
            <ButtonDefault
              bgcolor="#4AC34E"
              style={{ marginTop: 20 }}
              onPress={() => addProgress(dateFormat)}
              underlayColor="transparent"
            >
              <Text>MARCAR COMO FEITO</Text>
            </ButtonDefault>
          </>
        )}
        {!dayOff && !isFuture && !isDone && isToday && (
          <>
            <BigText>
              <Bold>HOJE TEM TREINO \o/</Bold>
            </BigText>
            <SmallText>Você tem {time} para treinar hoje.</SmallText>
            <ButtonDefault
              bgcolor="#4AC34E"
              style={{ marginTop: 20 }}
              onPress={goToWorkout}
              underlayColor="transparent"
            >
              <Text>INICIAR TREINO</Text>
            </ButtonDefault>
          </>
        )}
      </BallonArea>
    </>
  );
}

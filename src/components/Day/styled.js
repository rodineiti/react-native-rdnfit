import styled from 'styled-components/native';

export const Button = styled.TouchableHighlight`
  width: ${(props) => props.width}px;
  justify-content: center;
  align-items: center;
`;

export const Item = styled.View`
  width: 30px;
  height: 30px;
  border-radius: 15px;
  background-color: #999;
  justify-content: center;
  align-items: center;
`;

export const Text = styled.Text``;

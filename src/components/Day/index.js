import React from 'react';

import { Button, Item, Text } from './styled';

export default function Day({
  day,
  month,
  dailyProgress,
  workoutDays,
  dayW,
  onPress,
}) {
  let bgColor = '#f4f4f4';
  let opacity = 1;
  let today = new Date();
  today.setHours(0);
  today.setMinutes(0);
  today.setSeconds(0);
  today.setMilliseconds(0);

  let thisDate = new Date(today.getFullYear(), month, day);

  if (workoutDays.includes(thisDate.getDay())) {
    if (thisDate.getTime() < today.getTime()) {
      let thisYear = thisDate.getFullYear();
      let thisMonth = thisDate.getMonth() + 1;
      let thisDay = thisDate.getDate();
      if (thisMonth < 10) '0' + thisMonth;
      if (thisDay < 10) '0' + thisDay;

      let dateFormat = `${thisYear}-${thisMonth}-${thisDay}`;

      if (dailyProgress.includes(dateFormat)) {
        bgColor = '#B5FFB8'; // treinou
      } else {
        bgColor = '#FFB5B5'; // treinou
      }
    }
  } else {
    opacity = 0.2;
  }

  if (thisDate.getTime() === today.getTime()) {
    bgColor = '#b5eeff';
    opacity = 1;
  }

  return (
    <Button width={dayW} onPress={onPress} underlayColor="transparent">
      <Item
        style={{
          opacity,
          backgroundColor: bgColor,
        }}
      >
        <Text>{day}</Text>
      </Item>
    </Button>
  );
}

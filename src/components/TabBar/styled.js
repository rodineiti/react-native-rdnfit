import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
  flex-direction: row;
  background-color: #eee;
`;

export const Item = styled.View`
  flex: 1;
  height: 65px;
  align-items: center;
`;

export const Regular = styled.TouchableHighlight`
  align-items: center;
`;

export const Image = styled.Image`
  width: 25px;
  height: 25px;
  margin-top: 10px;
  margin-bottom: 5px;
`;

export const Text = styled.Text``;

export const Ball = styled.TouchableHighlight`
  width: 80px;
  height: 80px;
  background-color: #3ba237;
  border-radius: 40px;
  justify-content: center;
  align-items: center;
  border: 5px solid #fff;
  margin-top: -30px;
`;

export const BallImage = styled.Image`
  width: 40px;
  height: 40px;
`;

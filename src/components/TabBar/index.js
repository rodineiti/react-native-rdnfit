import React from 'react';

import {
  Container,
  Item,
  Regular,
  Image,
  Text,
  Ball,
  BallImage,
} from './styled';

export default function TabBar(props) {
  const focusedOptions =
    props.descriptors[props.state.routes[props.state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  return (
    <Container>
      {props.items.map((item) => (
        <Item key={item.route}>
          {item.type === 'regular' && (
            <Regular
              underlayColor="transparent"
              onPress={() => props.navigation.navigate(item.route)}
            >
              <>
                <Image source={item.icon} />
                <Text>{item.text}</Text>
              </>
            </Regular>
          )}
          {item.type === 'big' && (
            <Ball
              underlayColor="#00FF00"
              onPress={() => props.navigation.navigate(item.route)}
            >
              <BallImage source={item.icon} />
            </Ball>
          )}
        </Item>
      ))}
    </Container>
  );
}

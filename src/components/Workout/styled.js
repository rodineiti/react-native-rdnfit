import styled from 'styled-components/native';

export const Container = styled.View`
  background-color: #f1f1f1;
  flex-direction: row;
  border-radius: 10px;
  margin-bottom: 20px;
  border: 2px solid #ddd;
`;

export const Info = styled.View`
  flex: 1;
`;

export const Title = styled.Text`
  font-size: 17px;
  margin: 10px;
`;

export const Scroll = styled.ScrollView`
  margin: 10px;
`;

export const Actions = styled.View`
  justify-content: center;
  align-items: center;
`;

export const Btn = styled.TouchableHighlight`
  margin: 10px 10px;
`;

export const Muscle = styled.View`
  width: 40px;
  height: 40px;
  background-color: #ffcc98;
  border-radius: 5px;
  margin-right: 5px;
  justify-content: center;
  align-items: center;
`;

export const MuscleImage = styled.Image`
  width: 30px;
  height: 30px;
`;

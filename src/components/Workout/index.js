import React, { useState } from 'react';
import { MaterialIcons } from '@expo/vector-icons';

import {
  Container,
  Info,
  Title,
  Scroll,
  Actions,
  Btn,
  Muscle,
  MuscleImage,
} from './styled';
import useImageMuscle from '../useImageMuscle';

export default function Workout({
  data,
  handleAddItem,
  handleEditItem,
  handleDelete,
  handleGoAction,
}) {
  const [add, setAdd] = useState(false);
  let muscleGroups = [];
  for (let i in data.exercises) {
    if (!muscleGroups.includes(data.exercises[i].muscle)) {
      muscleGroups.push(data.exercises[i].muscle);
    }
  }

  const handleClick = () => {
    setAdd(!add);
    handleAddItem();
  };

  const handleEdit = () => {
    handleEditItem();
  };

  const handleDel = () => {
    handleDelete();
  };

  const handleGo = () => {
    handleGoAction();
  };

  return (
    <Container>
      <Info>
        <Title>ID: {data.id}</Title>
        <Title>Nome: {data.name}</Title>
        <Scroll horizontal={true}>
          {muscleGroups.map((muscle, key) => (
            <Muscle key={key}>
              <MuscleImage source={useImageMuscle(muscle)} />
            </Muscle>
          ))}
        </Scroll>
      </Info>
      <Actions>
        {handleAddItem && (
          <Btn onPress={() => handleClick()} underlayColor="transparent">
            {add ? (
              <MaterialIcons name="check" size={42} color="black" />
            ) : (
              <MaterialIcons name="add" size={42} color="black" />
            )}
          </Btn>
        )}

        {handleEditItem && (
          <Btn onPress={() => handleEdit()} underlayColor="transparent">
            <MaterialIcons name="edit" size={38} color="black" />
          </Btn>
        )}

        {handleDelete && (
          <Btn onPress={() => handleDel()} underlayColor="transparent">
            <MaterialIcons name="delete" size={38} color="black" />
          </Btn>
        )}

        {handleGoAction && (
          <Btn onPress={() => handleGo()} underlayColor="transparent">
            <MaterialIcons name="play-arrow" size={40} color="black" />
          </Btn>
        )}
      </Actions>
    </Container>
  );
}

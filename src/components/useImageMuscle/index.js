export default function useImageMuscle(imageTitle) {
  switch (imageTitle) {
    case 'abs':
      return require('./../../assets/muscles/abs.png');
    case 'back':
      return require('./../../assets/muscles/back.png');
    case 'biceps':
      return require('./../../assets/muscles/biceps.png');
    case 'chest':
      return require('./../../assets/muscles/chest.png');
    case 'gluteos':
      return require('./../../assets/muscles/gluteos.png');
    case 'legs':
      return require('./../../assets/muscles/legs.png');
    case 'shoulders':
      return require('./../../assets/muscles/shoulders.png');
    case 'triceps':
      return require('./../../assets/muscles/triceps.png');
    default:
      return null;
  }
}

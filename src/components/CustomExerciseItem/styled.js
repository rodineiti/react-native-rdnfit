import styled from 'styled-components/native';

export const Container = styled.View`
  height: 50px;
  flex-direction: row;
  margin-bottom: 10px;
`;

export const MuscleArea = styled.View`
  width: 50px;
  height: 50px;
  background-color: #ffcc98;
  border-radius: 10px;
  justify-content: center;
  align-items: center;
`;

export const Image = styled.Image`
  width: 35px;
  height: 35px;
`;

export const Info = styled.View`
  flex: 1;
  flex-direction: column;
  justify-content: center;
  margin-left: 5px;
`;

export const Name = styled.Text`
  font-size: 15px;
  color: #fff;
`;

export const Details = styled.Text`
  font-size: 15px;
  color: #999;
`;

export const Icon = styled.Image`
  width: 40px;
  height: 40px;
`;

export const Check = styled.TouchableHighlight`
  width: 60px;
  justify-content: center;
  align-items: center;
`;

export const View = styled.View`
  width: 40px;
  height: 40px;
  border: 5px solid #fff;
  border-radius: 20px;
`;

export const Count = styled.View`
  width: 25px;
  justify-content: center;
`;

export const Text = styled.Text`
  font-size: 17px;
  color: #fff;
`;

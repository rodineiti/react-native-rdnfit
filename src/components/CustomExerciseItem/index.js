import React from 'react';
import useImageMuscle from '../useImageMuscle';

import {
  Container,
  MuscleArea,
  Image,
  Info,
  Name,
  Details,
  Check,
  Icon,
  View,
  Count,
  Text,
} from './styled';

export default function CustomExerciseItem({ index, data, checkAction }) {
  return (
    <Container>
      <>
        <Count>
          <Text>{index + 1}.</Text>
        </Count>
        <MuscleArea>
          <Image source={useImageMuscle(data.muscle)} />
        </MuscleArea>
        <Info>
          <Name>{data.name}</Name>
          <Details>
            {`${data.sets} séries - ${data.reps} rep ${
              data.load ? `- ${data.load} kg` : ''
            }`}
          </Details>
        </Info>
        <Check onPress={checkAction} underlayColor="transparent">
          {data.done ? (
            <Icon source={require('./../../assets/check-white.png')} />
          ) : (
            <View></View>
          )}
        </Check>
      </>
    </Container>
  );
}

import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Started from './../screens/Started';
import StartedName from './../screens/StartedName';
import StartedDays from './../screens/StartedDays';
import StartedLevel from './../screens/StartedLevel';
import StartedRecomendations from './../screens/StartedRecomendations';

const Stack = createStackNavigator();

export default function AppStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Started"
        component={Started}
        options={{ headerShown: false }}
      />
      <Stack.Screen name="StartedName" component={StartedName} />
      <Stack.Screen name="StartedDays" component={StartedDays} />
      <Stack.Screen name="StartedLevel" component={StartedLevel} />
      <Stack.Screen
        name="StartedRecomendations"
        component={StartedRecomendations}
      />
    </Stack.Navigator>
  );
}

import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

import Preload from './../screens/Preload';
import AppStack from './AppStack';
import AppTab from './AppTab';

const Stack = createStackNavigator();

export default function MainStack() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Preload" headerMode="none">
        <Stack.Screen name="Preload" component={Preload} />
        <Stack.Screen name="AppStack" component={AppStack} />
        <Stack.Screen name="AppTab" component={AppTab} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

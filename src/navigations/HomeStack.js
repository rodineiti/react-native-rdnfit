import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Home from './../screens/Home';
import Config from './../screens/Config';

const Stack = createStackNavigator();

export default function HomeStack() {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen
        name="Config"
        component={Config}
        options={{ title: 'Configurações' }}
      />
    </Stack.Navigator>
  );
}

import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import HomeStack from './HomeStack';
import WorkoutStack from './WorkoutStack';
import MyWorkoutsStack from './MyWorkoutsStack';
import TabBar from '../components/TabBar';

const Tab = createBottomTabNavigator();

export default function AppTab() {
  return (
    <Tab.Navigator
      initialRouteName="HomeStack"
      tabBar={(props) => (
        <TabBar
          {...props}
          items={[
            {
              type: 'regular',
              text: 'Início',
              icon: require('./../assets/home.png'),
              route: 'HomeStack',
            },
            {
              type: 'big',
              icon: require('./../assets/dumbbell.png'),
              route: 'WorkoutStack',
            },
            {
              type: 'regular',
              text: 'Meus Treinos',
              icon: require('./../assets/myworkouts.png'),
              route: 'MyWorkoutsStack',
            },
          ]}
        />
      )}
    >
      <Tab.Screen name="HomeStack" component={HomeStack} />
      <Tab.Screen
        name="WorkoutStack"
        component={WorkoutStack}
        options={{
          tabBarVisible: false,
        }}
      />
      <Tab.Screen name="MyWorkoutsStack" component={MyWorkoutsStack} />
    </Tab.Navigator>
  );
}

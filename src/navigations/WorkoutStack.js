import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import WorkoutSelect from '../screens/WorkoutSelect';
import WorkoutChecklist from '../screens/WorkoutChecklist';

const Stack = createStackNavigator();

export default function WorkoutStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="WorkoutSelect" component={WorkoutSelect} />
      <Stack.Screen name="WorkoutChecklist" component={WorkoutChecklist} />
    </Stack.Navigator>
  );
}

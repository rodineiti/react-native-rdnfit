import React, { useLayoutEffect } from 'react';
import { CommonActions } from '@react-navigation/native';
import { useSelector } from 'react-redux';

import { Container, List, ButtonBack, Title } from './styled';
import Workout from './../../components/Workout';

export default function WorkoutSelect({ navigation }) {
  const myWorkouts = useSelector((state) => state.userReducer.myWorkouts);
  let lastWorkout =
    useSelector((state) => state.userReducer.lastWorkout) || false;

  if (lastWorkout) {
    lastWorkout = myWorkouts.find((i) => i.id === lastWorkout);
  }

  useLayoutEffect(() => {
    navigation.setOptions({
      title: 'Escolha seu treino',
      // eslint-disable-next-line react/display-name
      headerLeft: () => (
        <ButtonBack
          title="Voltar"
          onPress={() => {
            navigation.dispatch(
              CommonActions.reset({
                index: 0,
                routes: [{ name: 'AppTab' }],
              })
            );
          }}
        />
      ),
    });
  }, []);

  return (
    <Container>
      {lastWorkout && (
        <>
          <Title>Seu último treino foi:</Title>
          <Workout data={lastWorkout} />
        </>
      )}
      <Title>Escolha seu treino para hoje:</Title>
      <List
        data={myWorkouts}
        renderItem={({ item }) => (
          <Workout
            data={item}
            handleGoAction={() =>
              navigation.navigate('WorkoutChecklist', { workout: item })
            }
          />
        )}
        keyExtractor={(item) => String(item.id)}
      />
    </Container>
  );
}

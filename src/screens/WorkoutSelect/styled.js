import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
  flex: 1;
  margin: 10px 15px;
`;

export const Title = styled.Text`
  margin-bottom: 10px;
`;

export const List = styled.FlatList``;

export const ButtonBack = styled.Button`
  margin-left: 15px;
`;

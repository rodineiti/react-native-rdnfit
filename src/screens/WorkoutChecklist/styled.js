import styled from 'styled-components/native';

export const Container = styled.ImageBackground`
  flex: 1;
  background-color: #000;
  align-items: center;
`;

export const SafeArea = styled.SafeAreaView`
  flex: 1;
  width: 100%;
  align-items: center;
  background-color: rgba(1, 59, 14, 0.9);
`;

export const Header = styled.View`
  flex-direction: row;
  width: 90%;
  align-items: center;
  height: 70px;
`;

export const Title = styled.Text`
  flex: 1;
  color: #fff;
  font-size: 20px;
`;

export const Close = styled.TouchableHighlight`
  width: 50px;
  height: 50px;
  justify-content: center;
  align-items: center;
`;

export const Text = styled.Text`
  color: #fff;
  font-size: 22px;
  font-weight: bold;
`;

export const List = styled.FlatList`
  width: 90%;
  flex: 1;
`;

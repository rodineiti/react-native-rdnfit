import React, { useState, useLayoutEffect } from 'react';
import { CommonActions } from '@react-navigation/native';
import { StatusBar } from 'react-native';

import {
  Container,
  SafeArea,
  Header,
  Title,
  Close,
  Text,
  List,
} from './styled';
import CustomExerciseItem from './../../components/CustomExerciseItem';
import { useDispatch } from 'react-redux';

export default function WorkoutChecklist({ route, navigation }) {
  const dispatch = useDispatch();
  const workout =
    route.params && route.params.workout ? route.params.workout : false;
  const [exercises, setExercises] = useState([...workout.exercises]);

  useLayoutEffect(() => {
    navigation.setOptions({
      header: () => {
        return null;
      },
    });
  }, [workout]);

  const check = (item, index) => {
    let items = [...exercises]; // clonagem dos dados

    if (!item.done) {
      items[index].done = true;
    } else {
      items[index].done = false;
    }

    setExercises(items);

    checkAll();
  };

  const checkAll = () => {
    if (exercises.every((i) => i.done)) {
      // checa se todos foram chekados
      alert('PARABÉNS! Você finalizou todos os exercícios');

      let today = new Date();
      today.setHours(0);
      today.setMinutes(0);
      today.setSeconds(0);
      today.setMilliseconds(0);
      let thisYear = today.getFullYear();
      let thisMonth = today.getMonth() + 1;
      let thisDay = today.getDate();
      if (thisMonth < 10) '0' + thisMonth;
      if (thisDay < 10) '0' + thisDay;
      let dateFormat = `${thisYear}-${thisMonth}-${thisDay}`;

      addProgress(dateFormat);
      setLastWorkout(workout.id);

      navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{ name: 'AppTab' }],
        })
      );
    }
  };

  const addProgress = (date) => {
    dispatch({
      type: 'ADD_PROGRESS',
      payload: { date },
    });
  };

  const setLastWorkout = (id) => {
    dispatch({
      type: 'SET_LASTWORKOUT',
      payload: { id },
    });
  };

  return (
    <Container source={require('./../../assets/fitness.jpg')}>
      <StatusBar barStyle="light-content" />
      <SafeArea>
        <Header>
          <Title>{workout.name}</Title>
          <Close
            onPress={() => navigation.goBack()}
            underlayColor="transparent"
          >
            <Text>X</Text>
          </Close>
        </Header>
        <List
          data={exercises}
          renderItem={({ item, index }) => (
            <CustomExerciseItem
              index={index}
              data={item}
              checkAction={() => check(item, index)}
            />
          )}
          keyExtractor={(item) => String(item.id)}
        />
      </SafeArea>
    </Container>
  );
}

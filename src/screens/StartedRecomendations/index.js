import React, { useLayoutEffect } from 'react';
import { CommonActions } from '@react-navigation/native';
import { useSelector, useDispatch } from 'react-redux';
import workoutJson from './../../presetWorkouts.json';

import {
  Container,
  HeaderText,
  ButtonNext,
  ButtonBack,
  WorkoutList,
} from './styled';
import Workout from './../../components/Workout';

export default function StartedRecomendations({ navigation }) {
  const dispatch = useDispatch();
  const myWorkouts = useSelector((state) => state.userReducer.myWorkouts);

  const nextAction = () => {
    navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [{ name: 'AppTab' }],
      })
    );
  };

  useLayoutEffect(() => {
    navigation.setOptions({
      title: '',
      headerLeft: () => (
        <ButtonBack title="Voltar" onPress={() => navigation.goBack()} />
      ),
      headerRight: () => (
        <ButtonNext
          title={myWorkouts.length > 0 ? 'Concluir' : 'Ignorar'}
          onPress={nextAction}
        />
      ),
    });
  }, [myWorkouts]);

  const handleAddItem = (item) => {
    if (myWorkouts.findIndex((i) => i.id === item.id) < 0) {
      dispatch({
        type: 'SET_WORKOUT',
        payload: { workout: item },
      });
    } else {
      dispatch({
        type: 'DEL_WORKOUT',
        payload: { workout: item },
      });
    }
  };

  return (
    <Container>
      <HeaderText>Opções baseadas no seu nível.</HeaderText>
      <HeaderText>Você selecionou {myWorkouts.length} treinos</HeaderText>

      <WorkoutList
        data={workoutJson}
        renderItem={({ item }) => (
          <Workout data={item} handleAddItem={() => handleAddItem(item)} />
        )}
        keyExtractor={(item) => item.id}
      />
    </Container>
  );
}

import React, { useState, useLayoutEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  Container,
  HeaderText,
  NameInput,
  ButtonNext,
  ButtonBack,
} from './styled';

export default function StartedName({ navigation }) {
  const dispatch = useDispatch();
  const name = useSelector((state) => state.userReducer.name);
  const [text, setText] = useState(name || '');

  const nextAction = () => {
    if (!text) {
      alert('Favor, informe seu nome.');
    } else {
      dispatch({
        type: 'SET_NAME',
        payload: { name: text },
      });

      navigation.navigate('StartedDays');
    }
  };

  useLayoutEffect(() => {
    navigation.setOptions({
      title: '',
      // eslint-disable-next-line react/display-name
      headerLeft: () => (
        <ButtonBack title="Voltar" onPress={() => navigation.goBack()} />
      ),
      // eslint-disable-next-line react/display-name
      headerRight: () => <ButtonNext title="Próximo" onPress={nextAction} />,
    });
  }, [text]);

  return (
    <Container>
      <HeaderText>Qual o seu nome?</HeaderText>
      <NameInput
        value={text}
        onChangeText={(t) => setText(t)}
        autoFocus={true}
        autoCapitalize="words"
        onSubmitEditing={nextAction}
      />
    </Container>
  );
}

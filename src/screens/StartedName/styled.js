import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
  flex: 1;
  align-items: center;
  background-color: #fff;
`;

export const HeaderText = styled.Text`
  font-size: 22px;
  color: #333;
  margin-top: 50px;
  margin-bottom: 50px;
`;

export const NameInput = styled.TextInput`
  border: 1px solid #ccc;
  width: 90%;
  height: 50px;
  border-radius: 10px;
  font-size: 16px;
  padding: 10px;
  color: #000;
`;

export const ButtonNext = styled.Button`
  margin-right: 20px;
`;

export const ButtonBack = styled.Button`
  margin-left: 15px;
`;

import React from 'react';
import { StatusBar } from 'react-native';

import {
  Container,
  WelcomeText,
  WelcomeImage,
  WelcomeLogo,
  ConfigArea,
  ButtonText,
} from './styled';

import ButtonDefault from './../../components/ButtonDefault';

export default function Started({ navigation }) {
  const handleStart = () => {
    navigation.navigate('StartedName');
  };

  return (
    <Container>
      <StatusBar barStyle="light-content" />
      <WelcomeText>Bem vindo(a) ao RdnFit</WelcomeText>
      <WelcomeImage>
        <WelcomeLogo source={require('./../../assets/boneco.png')} />
      </WelcomeImage>
      <ConfigArea>
        <ButtonDefault
          width="90%"
          bgcolor="#0072C0"
          underlayColor="#0B7AC6"
          onPress={handleStart}
        >
          <ButtonText>Iniciar</ButtonText>
        </ButtonDefault>
      </ConfigArea>
    </Container>
  );
}

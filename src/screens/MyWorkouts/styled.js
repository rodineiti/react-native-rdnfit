import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
  flex: 1;
  margin: 10px 15px;
`;

export const List = styled.FlatList``;

export const ButtonAdd = styled.TouchableHighlight`
  margin-right: 10px;
`;

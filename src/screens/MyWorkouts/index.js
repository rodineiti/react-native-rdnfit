import React, { useLayoutEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { MaterialIcons } from '@expo/vector-icons';

import { Container, List, ButtonAdd } from './styled';
import Workout from './../../components/Workout';

export default function MyWorkouts({ navigation }) {
  const dispatch = useDispatch();
  const myWorkouts = useSelector((state) => state.userReducer.myWorkouts);

  useLayoutEffect(() => {
    navigation.setOptions({
      title: 'Meu treinos',
      headerLeft: () => null,
      // eslint-disable-next-line react/display-name
      headerRight: () => (
        <ButtonAdd onPress={() => navigation.navigate('EditWorkout')}>
          <MaterialIcons name="add" size={42} color="black" />
        </ButtonAdd>
      ),
    });
  }, []);

  const handleEdit = (workout) => {
    navigation.navigate('EditWorkout', { workout });
  };

  return (
    <Container>
      <List
        data={myWorkouts}
        renderItem={({ item }) => (
          <Workout
            data={item}
            handleEditItem={() => handleEdit(item)}
            handleDelete={() => {
              dispatch({
                type: 'DEL_WORKOUT',
                payload: { workout: item },
              });
            }}
          />
        )}
        keyExtractor={(item) => String(item.id)}
      />
    </Container>
  );
}

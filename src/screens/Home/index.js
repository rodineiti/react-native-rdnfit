import React, { useState, useLayoutEffect } from 'react';
import { StatusBar } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { FontAwesome } from '@expo/vector-icons';

import MonthScroll from './../../components/MonthScroll';
import DaysScroll from './../../components/DaysScroll';
import DayStatus from './../../components/DayStatus';

import {
  Container,
  ButtonConfig,
  Legend,
  LText,
  Item,
  Box,
  Text,
} from './styled';

const legends = [
  {
    name: 'hoje',
    description: 'Hoje',
    color: '#b5eeff',
    opacity: 1,
  },
  {
    name: 'feito',
    description: 'Treino feito',
    color: '#b5ffb8',
    opacity: 1,
  },
  {
    name: 'perdido',
    description: 'Treino perdido',
    color: '#ffb5b5',
    opacity: 1,
  },
  {
    name: 'descanso',
    description: 'Dia de descanso',
    color: '#f4f4f4',
    opacity: 0.2,
  },
  {
    name: 'futuro',
    description: 'Dia futuro',
    color: '#EEEEEE',
    opacity: 1,
  },
];

export default function Home({ navigation }) {
  const dispatch = useDispatch();
  let today = new Date();
  const [month, setMonth] = useState(today.getMonth());
  const [day, setDay] = useState(today.getDate());
  const dailyProgress = useSelector((state) => state.userReducer.dailyProgress);
  const workoutDays = useSelector((state) => state.userReducer.workoutDays);

  useLayoutEffect(() => {
    navigation.setOptions({
      title: 'Seu progresso diário',
      headerLeft: () => null,
      // eslint-disable-next-line react/display-name
      headerRight: () => (
        <ButtonConfig onPress={() => navigation.navigate('Config')}>
          <FontAwesome name="cog" size={24} color="black" />
        </ButtonConfig>
      ),
    });
  }, []);

  const addProgress = (date) => {
    dispatch({
      type: 'ADD_PROGRESS',
      payload: { date },
    });
  };

  const delProgress = (date) => {
    dispatch({
      type: 'DEL_PROGRESS',
      payload: { date },
    });
  };

  return (
    <Container>
      <StatusBar barStyle="dark-content" />
      <MonthScroll month={month} setMonth={setMonth} />
      <DaysScroll
        month={month}
        day={day}
        setDay={setDay}
        dailyProgress={dailyProgress}
        workoutDays={workoutDays}
      />
      <DayStatus
        month={month}
        day={day}
        setDay={setDay}
        dailyProgress={dailyProgress}
        workoutDays={workoutDays}
        addProgress={addProgress}
        delProgress={delProgress}
        goToWorkout={() => navigation.navigate('WorkoutStack')}
      />

      <Legend>
        <LText>Legenda:</LText>
        {legends.map((item) => (
          <Item key={item.name}>
            <Box
              style={{ backgroundColor: item.color, opacity: item.opacity }}
            ></Box>
            <Text>{item.description}</Text>
          </Item>
        ))}
      </Legend>
    </Container>
  );
}

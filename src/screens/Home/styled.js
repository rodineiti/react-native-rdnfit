import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
  align-items: center;
`;

export const ButtonConfig = styled.TouchableHighlight`
  margin-right: 20px;
`;

export const Legend = styled.View`
  width: 90%;
  align-items: flex-start;
  margin-top: 20px;
`;

export const LText = styled.Text`
  color: #555;
`;

export const Item = styled.View`
  flex-direction: row;
  align-items: center;
  margin-top: 5px;
`;

export const Box = styled.View`
  width: 15px;
  height: 15px;
  background-color: #ccc;
  margin-right: 5px;
`;

export const Text = styled.Text``;

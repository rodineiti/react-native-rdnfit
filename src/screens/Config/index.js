import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { CommonActions } from '@react-navigation/native';
import {
  Container,
  Title,
  NameInput,
  BoldText,
  DaysArea,
  DayText,
  LevelArea,
  LevelText,
} from './styled';
import ButtonDefault from './../../components/ButtonDefault';

export default function Config({ navigation }) {
  const dispatch = useDispatch();
  const name = useSelector((state) => state.userReducer.name) || '';
  const workoutDays =
    useSelector((state) => state.userReducer.workoutDays) || [];
  const level = useSelector((state) => state.userReducer.level) || null;

  const handleName = (name) => {
    if (!name) {
      alert('Favor, informe seu nome.');
    } else {
      dispatch({
        type: 'SET_NAME',
        payload: { name },
      });
    }
  };

  const handleLevel = (level) => {
    if (!level) {
      alert('Favor, informe seu nível.');
    } else {
      dispatch({
        type: 'SET_LEVEL',
        payload: { level },
      });
    }
  };

  const handleWorkout = (day) => {
    let newItems = [...workoutDays];

    if (!workoutDays.includes(day)) {
      newItems.push(day);
    } else {
      newItems = newItems.filter((item) => item !== day);
    }

    if (!newItems.length) {
      alert('Você precisa treinar ao menos um dia.');
    } else {
      dispatch({
        type: 'SET_WORKOUTDAYS',
        payload: { workoutDays: newItems },
      });
    }
  };

  const reset = () => {
    dispatch({
      type: 'RESET',
    });

    navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [{ name: 'AppStack' }],
      })
    );
  };

  return (
    <Container>
      <Title>Seu nome completo:</Title>
      <NameInput value={name} onChangeText={(t) => handleName(t)} />

      <Title>
        Quais os <BoldText>dias da semana</BoldText> você pretende treinar?
      </Title>
      <DaysArea>
        <ButtonDefault
          bgcolor={workoutDays.includes(1) ? '#A5E88C' : false}
          width={'100px'}
          style={{
            marginBottom: 20,
          }}
          onPress={() => handleWorkout(1)}
          underlayColor="#CCC"
        >
          <DayText>Segunda</DayText>
        </ButtonDefault>
        <ButtonDefault
          bgcolor={workoutDays.includes(2) ? '#A5E88C' : false}
          width={'100px'}
          style={{
            marginBottom: 20,
          }}
          onPress={() => handleWorkout(2)}
          underlayColor="#CCC"
        >
          <DayText>Terça</DayText>
        </ButtonDefault>
        <ButtonDefault
          bgcolor={workoutDays.includes(3) ? '#A5E88C' : false}
          width={'100px'}
          style={{
            marginBottom: 20,
          }}
          onPress={() => handleWorkout(3)}
          underlayColor="#CCC"
        >
          <DayText>Quarta</DayText>
        </ButtonDefault>
        <ButtonDefault
          bgcolor={workoutDays.includes(4) ? '#A5E88C' : false}
          width={'100px'}
          style={{
            marginBottom: 20,
          }}
          onPress={() => handleWorkout(4)}
          underlayColor="#CCC"
        >
          <DayText>Quinta</DayText>
        </ButtonDefault>
        <ButtonDefault
          bgcolor={workoutDays.includes(5) ? '#A5E88C' : false}
          width={'100px'}
          style={{
            marginBottom: 20,
          }}
          onPress={() => handleWorkout(5)}
          underlayColor="#CCC"
        >
          <DayText>Sexta</DayText>
        </ButtonDefault>
        <ButtonDefault
          bgcolor={workoutDays.includes(6) ? '#A5E88C' : false}
          width={'100px'}
          style={{
            marginBottom: 20,
          }}
          onPress={() => handleWorkout(6)}
          underlayColor="#CCC"
        >
          <DayText>Sábado</DayText>
        </ButtonDefault>
        <ButtonDefault
          bgcolor={workoutDays.includes(0) ? '#A5E88C' : false}
          width={'100px'}
          style={{
            marginBottom: 20,
          }}
          onPress={() => handleWorkout(0)}
          underlayColor="#CCC"
        >
          <DayText>Domingo</DayText>
        </ButtonDefault>
      </DaysArea>

      <Title>
        <BoldText>Qual seu nível?</BoldText>
      </Title>
      <LevelArea>
        <ButtonDefault
          bgcolor={level === 'beginner' ? '#A5E88C' : false}
          style={{
            marginBottom: 20,
          }}
          onPress={() => handleLevel('beginner')}
          underlayColor="#CCC"
        >
          <LevelText>Iniciante</LevelText>
        </ButtonDefault>
        <ButtonDefault
          bgcolor={level === 'intermediate' ? '#A5E88C' : false}
          style={{
            marginBottom: 20,
          }}
          onPress={() => handleLevel('intermediate')}
          underlayColor="#CCC"
        >
          <LevelText>Intermediário</LevelText>
        </ButtonDefault>
        <ButtonDefault
          bgcolor={level === 'advanced' ? '#A5E88C' : false}
          style={{
            marginBottom: 20,
          }}
          onPress={() => handleLevel('advanced')}
          underlayColor="#CCC"
        >
          <LevelText>Avançado</LevelText>
        </ButtonDefault>
      </LevelArea>

      <Title>
        Você quer <BoldText>Resetar</BoldText> tudo?
      </Title>
      <ButtonDefault bgcolor={'#ff0000'} onPress={reset} underlayColor="#CCC">
        <LevelText style={{ color: '#ffffff', fontWeight: 'bold' }}>
          Resetar tudo
        </LevelText>
      </ButtonDefault>
    </Container>
  );
}

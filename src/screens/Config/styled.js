import styled from 'styled-components/native';

export const Container = styled.ScrollView`
  flex: 1;
  margin: 0 15px;
`;

export const Title = styled.Text`
  font-size: 15px;
  font-weight: bold;
  color: #333;
  margin-top: 20px;
  margin-bottom: 20px;
`;

export const NameInput = styled.TextInput`
  border: 1px solid #ccc;
  width: 100%;
  height: 50px;
  border-radius: 10px;
  font-size: 16px;
  padding: 10px;
  color: #000;
`;

export const BoldText = styled.Text`
  font-weight: bold;
`;

export const DayText = styled.Text``;

export const DaysArea = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
  margin-right: 10px;
  margin-left: 10px;
`;

export const LevelArea = styled.View`
  flex-direction: row;
  justify-content: center;
`;

export const LevelText = styled.Text``;

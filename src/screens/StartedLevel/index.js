import React, { useState, useEffect, useLayoutEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  Container,
  HeaderText,
  ButtonNext,
  ButtonBack,
  LevelText,
  LevelArea,
  BoldText,
} from './styled';
import ButtonDefault from './../../components/ButtonDefault';

export default function StartedLevel({ navigation }) {
  const dispatch = useDispatch();
  const workoutDays = useSelector((state) => state.userReducer.workoutDays);
  const [level, setLevel] = useState(
    useSelector((state) => state.userReducer.level) || null
  );
  const [text, setText] = useState('');

  const nextAction = () => {
    if (!level) {
      alert('Selecione seu nível.');
    } else {
      dispatch({
        type: 'SET_LEVEL',
        payload: { level },
      });

      navigation.navigate('StartedRecomendations');
    }
  };

  useLayoutEffect(() => {
    navigation.setOptions({
      title: '',
      // eslint-disable-next-line react/display-name
      headerLeft: () => (
        <ButtonBack title="Voltar" onPress={() => navigation.goBack()} />
      ),
      // eslint-disable-next-line react/display-name
      headerRight: () => <ButtonNext title="Próximo" onPress={nextAction} />,
    });
  }, [level]);

  useEffect(() => {
    switch (workoutDays.length) {
      case 1:
        setText('Só 1? Você quem sabe...');
        break;
      case 2:
        setText('2 dias, ainda acho pouco, mas tudo bem.');
        break;
      case 3:
        setText('3 dias, legal.');
        break;
      case 4:
        setText('4 dias, bom.');
        break;
      case 5:
        setText('5 dias, muito bom.');
        break;
      case 6:
        setText('6 dias, very good.');
        break;
      case 7:
        setText('7 dias, tu eh fodão.');
        break;
      default:
        setText('...');
    }
  }, [level]);

  return (
    <Container>
      <HeaderText>{text}</HeaderText>
      <HeaderText>
        <BoldText>Qual o seu nível hoje?</BoldText>
      </HeaderText>
      <LevelArea>
        <ButtonDefault
          bgcolor={level === 'beginner' ? '#A5E88C' : false}
          style={{
            marginBottom: 20,
          }}
          onPress={() => setLevel('beginner')}
          underlayColor="#CCC"
        >
          <LevelText>Iniciante</LevelText>
        </ButtonDefault>
        <ButtonDefault
          bgcolor={level === 'intermediate' ? '#A5E88C' : false}
          style={{
            marginBottom: 20,
          }}
          onPress={() => setLevel('intermediate')}
          underlayColor="#CCC"
        >
          <LevelText>Intermediário</LevelText>
        </ButtonDefault>
        <ButtonDefault
          bgcolor={level === 'advanced' ? '#A5E88C' : false}
          style={{
            marginBottom: 20,
          }}
          onPress={() => setLevel('advanced')}
          underlayColor="#CCC"
        >
          <LevelText>Avançado</LevelText>
        </ButtonDefault>
      </LevelArea>
    </Container>
  );
}

import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
  flex: 1;
  align-items: center;
  background-color: #fff;
`;

export const HeaderText = styled.Text`
  font-size: 22px;
  color: #333;
  margin-top: 50px;
`;

export const ButtonNext = styled.Button`
  margin-right: 20px;
`;

export const ButtonBack = styled.Button`
  margin-left: 15px;
`;

export const LevelText = styled.Text``;

export const LevelArea = styled.View`
  width: 100%;
  padding-right: 10px;
  padding-left: 10px;
  margin-top: 50px;
`;

export const BoldText = styled.Text`
  font-weight: bold;
`;

import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
  flex: 1;
  margin: 10px 15px;
`;

export const Text = styled.Text`
  color: #fff;
  font-weight: bold;
`;

export const ButtonSave = styled.TouchableHighlight`
  margin-right: 20px;
`;

export const NameInput = styled.TextInput`
  border: 1px solid #ccc;
  width: 100%;
  height: 50px;
  border-radius: 10px;
  font-size: 16px;
  padding: 10px;
  color: #000;
`;

export const Area = styled.View`
  flex: 1;
  margin-top: 20px;
  padding-top: 20px;
  border-top-width: 1px;
  border-top-color: #ccc;
`;

export const List = styled.FlatList`
  flex: 1;
  padding-top: 20px;
`;

export const ModalLabel = styled.Text`
  font-size: 15px;
  font-weight: bold;
  margin-top: 10px;
`;

export const ModalMuscles = styled.ScrollView``;

export const Muscle = styled.TouchableHighlight`
  width: 50px;
  height: 50px;
  background-color: #eee;
  border-radius: 10px;
  justify-content: center;
  align-items: center;
  margin-right: 10px;
  opacity: ${(props) => props.opacity || 1};
`;

export const Image = styled.Image`
  width: 35px;
  height: 35px;
`;

export const ModalExtra = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: 20px;
`;

export const ModalExtraItem = styled.View`
  align-items: center;
  width: 33.3%;
  padding: 0 4px;
`;

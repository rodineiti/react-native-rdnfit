import React, { useLayoutEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { FontAwesome } from '@expo/vector-icons';

import {
  Container,
  Text,
  ButtonSave,
  NameInput,
  Area,
  List,
  ModalLabel,
  ModalMuscles,
  Muscle,
  Image,
  ModalExtra,
  ModalExtraItem,
} from './styled';
import ButtonDefault from './../../components/ButtonDefault';
import ExerciseItem from '../../components/ExerciseItem';
import ModalDefault from '../../components/ModalDefault';

export default function EditWorkout({ route, navigation }) {
  const workout =
    route.params && route.params.workout ? route.params.workout : false;

  const [id, setId] = useState(workout ? workout.id : false);
  const [name, setName] = useState(workout ? workout.name : '');
  const [exercises, setExercises] = useState(
    workout.exercises ? workout.exercises : []
  );
  const [modalShow, setModalShow] = useState(false);
  const [modalId, setModalId] = useState('');
  const [modalName, setModalName] = useState('');
  const [modalMuscle, setModalMuscle] = useState('');
  const [modalSets, setModalSets] = useState('');
  const [modalReps, setModalReps] = useState('');
  const [modalLoad, setModalLoad] = useState('');
  const dispatch = useDispatch();

  useLayoutEffect(() => {
    navigation.setOptions({
      title:
        route.params && route.params.workout
          ? 'Editar Treino'
          : 'Adicionar Treino',
      // eslint-disable-next-line react/display-name
      headerRight: () => (
        <ButtonSave onPress={saveAction}>
          <FontAwesome name="save" size={30} color="black" />
        </ButtonSave>
      ),
    });
  }, [workout, id, name, exercises]);

  const saveAction = () => {
    if (name === '') {
      alert('Você precisa informar o nome do exercídio');
      return;
    }

    if (exercises.length <= 0) {
      alert('Você precisa ter pelo menos 1 exercício.');
      return;
    }

    if (id) {
      dispatch({
        type: 'EDIT_WORKOUT',
        payload: {
          workout: {
            id,
            name,
            exercises,
          },
        },
      });
    } else {
      dispatch({
        type: 'SET_WORKOUT',
        payload: {
          workout: {
            id: new Date().getTime(),
            name,
            exercises,
          },
        },
      });
    }

    navigation.goBack();
  };

  const editAction = (exercise) => {
    setModalId(exercise.id);
    setModalName(exercise.name);
    setModalMuscle(exercise.muscle);
    setModalSets(exercise.sets);
    setModalReps(exercise.reps);
    setModalLoad(exercise.load);
    setModalShow(true);
  };

  const delAction = (exercise) => {
    let newItems = [...exercises];
    newItems = newItems.filter((i) => i.id !== exercise.id);
    setExercises(newItems);
  };

  const addItem = () => {
    resetModal();
    setModalShow(true);
  };

  const modalSave = () => {
    let newItems = [...exercises];

    if (
      modalName === '' ||
      modalMuscle === '' ||
      modalSets == '' ||
      modalReps == ''
    ) {
      alert('Preencha todas as informações.');
      return;
    }

    if (modalId !== '') {
      let index = newItems.findIndex((i) => i.id === modalId);
      if (index > -1) {
        newItems[index].name = modalName;
        newItems[index].muscle = modalMuscle;
        newItems[index].sets = modalSets;
        newItems[index].reps = modalReps;
        newItems[index].load = modalLoad;
      }
    } else {
      let newId = new Date().getTime();
      newItems.push({
        id: newId,
        name: modalName,
        muscle: modalMuscle,
        sets: modalSets,
        reps: modalReps,
        load: modalLoad,
      });
    }

    setExercises(newItems);
    setModalShow(false);
  };

  const resetModal = () => {
    setModalId('');
    setModalName('');
    setModalMuscle('');
    setModalSets('');
    setModalReps('');
    setModalLoad('');
  };

  return (
    <Container>
      <ModalDefault visible={modalShow} closeAction={() => setModalShow(false)}>
        <ModalLabel>Músculo de foco</ModalLabel>
        <ModalMuscles horizontal={true} showsHorizontalScrollIndicator={false}>
          <Muscle
            opacity={modalMuscle === 'abs' ? 1 : 0.3}
            onPress={() => setModalMuscle('abs')}
            underlayColor="transparent"
          >
            <Image source={require('./../../assets/muscles/abs.png')} />
          </Muscle>
          <Muscle
            opacity={modalMuscle === 'back' ? 1 : 0.3}
            onPress={() => setModalMuscle('back')}
            underlayColor="transparent"
          >
            <Image source={require('./../../assets/muscles/back.png')} />
          </Muscle>
          <Muscle
            opacity={modalMuscle === 'biceps' ? 1 : 0.3}
            onPress={() => setModalMuscle('biceps')}
            underlayColor="transparent"
          >
            <Image source={require('./../../assets/muscles/biceps.png')} />
          </Muscle>
          <Muscle
            opacity={modalMuscle === 'chest' ? 1 : 0.3}
            onPress={() => setModalMuscle('chest')}
            underlayColor="transparent"
          >
            <Image source={require('./../../assets/muscles/chest.png')} />
          </Muscle>
          <Muscle
            opacity={modalMuscle === 'gluteos' ? 1 : 0.3}
            onPress={() => setModalMuscle('gluteos')}
            underlayColor="transparent"
          >
            <Image source={require('./../../assets/muscles/gluteos.png')} />
          </Muscle>
          <Muscle
            opacity={modalMuscle === 'legs' ? 1 : 0.3}
            onPress={() => setModalMuscle('legs')}
            underlayColor="transparent"
          >
            <Image source={require('./../../assets/muscles/legs.png')} />
          </Muscle>
          <Muscle
            opacity={modalMuscle === 'shoulders' ? 1 : 0.3}
            onPress={() => setModalMuscle('shoulders')}
            underlayColor="transparent"
          >
            <Image source={require('./../../assets/muscles/shoulders.png')} />
          </Muscle>
          <Muscle
            opacity={modalMuscle === 'triceps' ? 1 : 0.3}
            onPress={() => setModalMuscle('triceps')}
            underlayColor="transparent"
          >
            <Image source={require('./../../assets/muscles/triceps.png')} />
          </Muscle>
        </ModalMuscles>

        <ModalLabel>Nome do exercício</ModalLabel>
        <NameInput
          value={modalName}
          onChangeText={(t) => setModalName(t)}
          autoCapitalize="words"
          placeholder="Digite o nome do exercício"
        />
        <ModalExtra>
          <ModalExtraItem>
            <ModalLabel>Séries</ModalLabel>
            <NameInput
              keyboardType="numeric"
              value={modalSets}
              onChangeText={(t) => setModalSets(t)}
              autoCapitalize="words"
              placeholder="Digite quantas séries"
            />
          </ModalExtraItem>
          <ModalExtraItem>
            <ModalLabel>Repetições</ModalLabel>
            <NameInput
              keyboardType="numeric"
              value={modalReps}
              onChangeText={(t) => setModalReps(t)}
              autoCapitalize="words"
              placeholder="Digite quantas repetições"
            />
          </ModalExtraItem>
          <ModalExtraItem>
            <ModalLabel>Carga</ModalLabel>
            <NameInput
              keyboardType="numeric"
              value={modalLoad}
              onChangeText={(t) => setModalLoad(t)}
              autoCapitalize="words"
              placeholder="Digite o total de carga"
            />
          </ModalExtraItem>
        </ModalExtra>

        <ButtonDefault
          bgcolor="#4ac34e"
          onPress={modalSave}
          underlayColor="transparent"
        >
          <Text>Salvar</Text>
        </ButtonDefault>
      </ModalDefault>

      <NameInput
        value={name}
        onChangeText={(t) => setName(t)}
        autoCapitalize="words"
        placeholder="Digite o nome do treino"
      />

      <Area>
        <ButtonDefault
          bgcolor="#4ac34e"
          onPress={addItem}
          underlayColor="transparent"
        >
          <Text>Adicionar exercício</Text>
        </ButtonDefault>

        <List
          data={exercises}
          renderItem={({ item }) => (
            <ExerciseItem
              data={item}
              delAction={() => delAction(item)}
              editAction={() => editAction(item)}
            />
          )}
          keyExtractor={(item) => item.name}
        />
      </Area>
    </Container>
  );
}

import React, { useState, useLayoutEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  Container,
  HeaderText,
  ButtonNext,
  ButtonBack,
  BoldText,
  DayText,
  DaysArea,
} from './styled';
import ButtonDefault from './../../components/ButtonDefault';

export default function StartedDays({ navigation }) {
  const dispatch = useDispatch();
  const name = useSelector((state) => state.userReducer.name);
  const workoutDays = useSelector((state) => state.userReducer.workoutDays);
  const [workout, setWorkout] = useState(workoutDays || []);

  const toggle = (day) => {
    let newItem = [...workout];

    if (!workout.includes(day)) {
      newItem.push(day);
    } else {
      newItem = newItem.filter((item) => item !== day);
    }

    setWorkout(newItem);
  };

  const nextAction = () => {
    if (!workout.length) {
      alert('Selecione ao menos um dia da semana.');
    } else {
      dispatch({
        type: 'SET_WORKOUTDAYS',
        payload: { workoutDays: workout },
      });

      navigation.navigate('StartedLevel');
    }
  };

  useLayoutEffect(() => {
    navigation.setOptions({
      title: '',
      headerLeft: () => (
        <ButtonBack title="Voltar" onPress={() => navigation.goBack()} />
      ),
      headerRight: () => <ButtonNext title="Próximo" onPress={nextAction} />,
    });
  }, [workout]);

  return (
    <Container>
      <HeaderText>
        Olá <BoldText>{name}</BoldText>, tudo bem?
      </HeaderText>
      <HeaderText top="-10">
        Quais os <BoldText>dias da semana</BoldText> você pretende treinar?
      </HeaderText>
      <DaysArea>
        <ButtonDefault
          bgcolor={workout.includes(1) ? '#A5E88C' : false}
          width={'100px'}
          style={{
            marginBottom: 20,
          }}
          onPress={() => toggle(1)}
          underlayColor="#CCC"
        >
          <DayText>Segunda</DayText>
        </ButtonDefault>
        <ButtonDefault
          bgcolor={workout.includes(2) ? '#A5E88C' : false}
          width={'100px'}
          style={{
            marginBottom: 20,
          }}
          onPress={() => toggle(2)}
          underlayColor="#CCC"
        >
          <DayText>Terça</DayText>
        </ButtonDefault>
        <ButtonDefault
          bgcolor={workout.includes(3) ? '#A5E88C' : false}
          width={'100px'}
          style={{
            marginBottom: 20,
          }}
          onPress={() => toggle(3)}
          underlayColor="#CCC"
        >
          <DayText>Quarta</DayText>
        </ButtonDefault>
        <ButtonDefault
          bgcolor={workout.includes(4) ? '#A5E88C' : false}
          width={'100px'}
          style={{
            marginBottom: 20,
          }}
          onPress={() => toggle(4)}
          underlayColor="#CCC"
        >
          <DayText>Quinta</DayText>
        </ButtonDefault>
        <ButtonDefault
          bgcolor={workout.includes(5) ? '#A5E88C' : false}
          width={'100px'}
          style={{
            marginBottom: 20,
          }}
          onPress={() => toggle(5)}
          underlayColor="#CCC"
        >
          <DayText>Sexta</DayText>
        </ButtonDefault>
        <ButtonDefault
          bgcolor={workout.includes(6) ? '#A5E88C' : false}
          width={'100px'}
          style={{
            marginBottom: 20,
          }}
          onPress={() => toggle(6)}
          underlayColor="#CCC"
        >
          <DayText>Sábado</DayText>
        </ButtonDefault>
        <ButtonDefault
          bgcolor={workout.includes(0) ? '#A5E88C' : false}
          width={'100px'}
          style={{
            marginBottom: 20,
          }}
          onPress={() => toggle(0)}
          underlayColor="#CCC"
        >
          <DayText>Domingo</DayText>
        </ButtonDefault>
      </DaysArea>
    </Container>
  );
}

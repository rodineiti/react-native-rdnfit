import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
  flex: 1;
  align-items: center;
  background-color: #fff;
`;

export const HeaderText = styled.Text`
  font-size: 16px;
  color: #333;
  margin-top: ${({ top }) => top || 10}px;
  margin-bottom: 50px;
`;

export const ButtonNext = styled.Button`
  margin-right: 20px;
`;

export const ButtonBack = styled.Button`
  margin-left: 15px;
`;

export const BoldText = styled.Text`
  font-weight: bold;
`;

export const DayText = styled.Text``;

export const DaysArea = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
  margin-right: 10px;
  margin-left: 10px;
`;

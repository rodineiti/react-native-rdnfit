import { useEffect } from 'react';
import { CommonActions } from '@react-navigation/native';
import { useSelector } from 'react-redux';

export default function Preload({ navigation }) {
  const name = useSelector((state) => state.userReducer.name);

  useEffect(() => {
    if (!name) {
      // redirect stack
      navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{ name: 'AppStack' }],
        })
      );
    } else {
      // redirect tab
      navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{ name: 'AppTab' }],
        })
      );
    }
  }, [name]);

  return null;
}
